###################################################
# Main experiment configuration file
# the file is automatically sourced by the scripts
##################################################
# name of the experiments
exp_name="abuk1"

# directory where to find the configuration folders
TOPDIR="../"

# Main parameters for the adaptation loop
# here on sub interval k is 10y
# The total simulation will be jmax*kmax years
# allowing jmax adaptation loops for each interval
imax=10
jmax=5
kmax=1

########################################
# numbers of partitions
#########################################
# for running elmer together with xios
# => abuk_i_j_k.sh
np1=126
nxios=2

# for running elmer alone
# => init_i_j.sh
np2=64

########################################################
# Initial mesh and state file (with zs and ssavelocity)
#######################################################"
INIT_MESH="../DATA/MSH"
INIT_FILE="../DATA/restart_000.nc"

# Should we done a state mesh adaptation from the 
# initial state file and mesh
first_adapt=true


##################################
# Function to run Elmer
# specific to the clusters
##################################
# run elmer with xios
# parameters :
#  $1 : number of partitions for elmer
#  $2 : number of partitions for xios
run_elmer_xios (){
  echo 
  echo "RUNNING ELMER:"
  echo "$1 ElmerSolver_mpi"  > mpmd.conf
  echo "$2 ./xios_server.exe" >> mpmd.conf
  ccc_mprun -f mpmd.conf
}

# running elmer alone
# parameters :
#  $1 : number of partitions for elmer
run_elmer (){
	echo
        echo "RUNNING ELMER:"
        ccc_mprun -n $1 ElmerSolver_mpi
}

######################################
# JOB submission
# specific to the clusters
# submit a job to run step <i> <j> <k>
# parameters:
# - $1: <i>
# - $2: <j>
# - $3: <k>
######################################
submit_next_job (){
   #walltime=18000
   ## walltime as a fn of number of nodes
   ## try 6h for 600 knodes (for 10y)
   nnodes=$(echo $(head -n 1 MSH_"$1"_"$2"/mesh.header | awk '{print $1}'))
   walltime=$((6*3600*$nnodes/600000))

   sed -s "s/<II>/$1/g;s/<JJ>/$2/g;s/<KK>/$3/g" $TOPDIR/MSUB/Job.tp.sh > Job_"$1"_"$2"_"$3".sh
   ccc_msub -T $walltime Job_"$1"_"$2"_"$3".sh
}

#################################################
# Implement a convergence test for th emesh adaptation <j>-loop
#  e.g. converged is true is the number of nodes from the last iteration
#  has changed by less than 5%
#
# parameters:
# - $1: currengt setp <i>
# - $2: current step <j>
#################################################
converged (){
## tolerance
tol=0.05

ii=$1
jj=$2

echo 
echo "convergence: ###########"

if [[ "$jj" -le 1 ]];then
	echo "convergence: first iteration; return"
	echo "convergence:  ###########"
	echo
	return 1
fi

mesh1=MSH_"$ii"_"$jj"
mesh2=MSH_"$ii"_"$((jj-1))"

result=$(echo $(head -n 1 $mesh1/mesh.header | awk '{print $1}')  $(head -n 1 $mesh2/mesh.header | awk '{print $1}') | awk '{print 2*sqrt(($1-$2)*($1-$2))/($1+$2)}')

if awk "BEGIN {exit !($result >= $tol)}"; then
     echo "convergence: relative change $result"
     echo "convergence: not converged"
     echo "convergence:  ###########"
     echo
     return 1
else
     echo "convergence: relative change $result"
     echo "convergence: converged"
     echo "convergence:  ###########"
     echo
     return 0
fi
}

