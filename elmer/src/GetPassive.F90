     !------------------------------------------------------------------------------
      SUBROUTINE GetPassive_Init0(Model,Solver,dt,Transient)
     !------------------------------------------------------------------------------
      USE DefUtils
      IMPLICIT NONE
     !------------------------------------------------------------------------------
      TYPE(Solver_t) :: Solver
      TYPE(Model_t) :: Model
      REAL(KIND=dp) :: dt
      LOGICAL :: Transient
     !------------------------------------------------------------------------------
      TYPE(ValueList_t), POINTER :: SolverParams
      REAL(KIND=dp) :: preal
      INTEGER :: pint
      LOGICAL :: Found

      REAL(KIND=dp),PARAMETER :: AreaThreshold=1.0e7
      INTEGER,PARAMETER :: NoEThreshold=20

        SolverParams => GetSolverParams(Solver)

        preal=ListGetConstReal(SolverParams,"Area Lower Limit",Found)
        IF (.NOT.Found) &
           CALL ListAddConstReal(SolverParams,"Area Lower Limit",AreaThreshold)

        pint=ListGetInteger(SolverParams,"Element Lower Limit",Found)
        IF (.NOT.Found) &
           CALL ListAddInteger(SolverParams,"Element Lower Limit",NoEThreshold)


      END SUBROUTINE GetPassive_Init0
!------------------------------------------------------------------------------
!
!------------------------------------------------------------------------------
      SUBROUTINE GetPassive( Model,Solver,dt,TransientSimulation)
      USE DefUtils
      USE ConnectedAreas
      IMPLICIT NONE

      TYPE(Model_t) :: Model
      TYPE(Solver_t), TARGET :: Solver
      REAL(KIND=dp) :: dt
      LOGICAL :: TransientSimulation

      TYPE(ValueList_t), POINTER :: SolverParams
      TYPE(Mesh_t), POINTER :: Mesh
      TYPE(Element_t), POINTER :: Element
      TYPE(Variable_t), POINTER :: VarR,VarP,Area,NoE
      TYPE(Region_t), ALLOCATABLE :: Regions(:),UniqueRegions(:),SerialRegions(:)
      REAL(Kind=dp) ::  largest,smallest
      INTEGER :: n
      INTEGER :: nSize
      INTEGER :: t
      INTEGER :: EIndex,kR,kP,k
      INTEGER :: region

      INTEGER :: localNoE
      REAL(KIND=dp) :: localArea

      INTEGER :: NoEThreshold 
      REAL(KIND=dp) :: AreaThreshold

      CHARACTER(LEN=MAX_NAME_LEN),PARAMETER :: VarName="RegionLabels"
      CHARACTER(*),PARAMETER :: SolverName="Testp"

      !-------------------------------------------------------------------
      SolverParams => GetSolverParams(Solver)
      AreaThreshold=ListGetConstReal(SolverParams,"Area Lower Limit",UnFoundFatal=.TRUE.)
      NoEThreshold=ListGetInteger(SolverParams,"Element Lower Limit",UnFoundFatal=.TRUE.)


      Mesh => Solver % Mesh

      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      CALL GetConnected(Solver,Regions,UniqueRegions, &
              VarName,CreateAux=.TRUE.)

      n = SIZE(UniqueRegions)
      WRITE(Message,'(A)') 'There is '//I2S(n)//' unconnected areas'
      CALL INFO(SolverName,Message,level=3)

      largest=MAXVAL(UniqueRegions(:) % Area)
      smallest=MINVAL(UniqueRegions(:) % Area)
      WRITE(Message,'(A,e15.7)') 'largest area :',largest
      CALL INFO(SolverName,Message,level=3)
      WRITE(Message,'(A,e15.7)') 'smallest area :',smallest
      CALL INFO(SolverName,Message,level=3)


      VarR =>  VariableGet( Mesh % Variables,TRIM(VarName),UnfoundFatal=.True.)

      Area =>  VariableGet( Mesh % Variables,TRIM(VarName)//"_Area",UnfoundFatal=.True.)
      NoE =>  VariableGet( Mesh % Variables,TRIM(VarName)//"_NoE",UnfoundFatal=.True.)

      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! Create Passive Variable
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      nSize= Mesh % NumberOfBulkElements
      VarP => EVarGet(Solver,"Passive",nSize,.TRUE.)
      VarP % Values = -1._dp

      DO t=1,nSize
         Element  => Mesh % Elements(t)
         EIndex= Element % ElementIndex

         k=EIndex
         IF (ASSOCIATED(VarR % Perm)) k = VarR % Perm(k)
         IF (k.LE.0) &
            CALL FATAL(SolverName,"invalide permutation "//I2S(t))
         region= VarR % Values(k)

         kP = EIndex
         IF (ASSOCIATED(VarP % Perm)) kP = VarP % Perm(kP)
         IF (kP.LE.0) &
            CALL FATAL(SolverName,"invalide permutation "//I2S(t))

         IF (region.LE.0) THEN
            VarP % Values(kP) = +1._dp
         ELSE
            k=EIndex
            IF (ASSOCIATED(Area%Perm)) k=Area%Perm(k)
            IF (k.GT.0) localArea=Area%values(k)
                
            k=EIndex
            IF (ASSOCIATED(NoE%Perm)) k=NoE%Perm(k)
            IF (k.GT.0) localNoE=INT(NoE%values(k))

            IF ((localArea < AreaThreshold) .OR. &
                (localNoE < NoEThreshold)) THEN
               VarP % Values(kP) = +1._dp
            ELSE
               VarP % Values(kP) = -1._dp
            END IF
         ENDIF
      END DO

      END SUBROUTINE GetPassive
