!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!     Active/Passice condition for abukill
!!      return +1 if groundedmask < glim=-0.5_dp ; i.e. floating node
!!
!!   USAGE:
!!      gp Passive =  Variable groundedmask
!!       Real Procedure "USFs" "abuk"
!       Passive Element Min Nodes = Integer 3
!!
!! => element (303) will be passive if all (3) nodes are floating
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      FUNCTION abuk(Model,nodenumber,groundedmask)  RESULT(passive)
       USE Types
       implicit none
       !-----------------
       TYPE(Model_t) :: Model
       INTEGER :: nodenumber
       REAL(kind=dp) :: groundedmask
       REAL(kind=dp) :: passive
       !-----------------
       REAL(kind=dp),PARAMETER :: glim=-0.5_dp

       IF (groundedmask.LT.glim) THEN
          passive=1._dp
       ELSE
          passive=-1._dp
       END IF

      END FUNCTION abuk

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!     Conditionnal Dirichlet for H
!!      return a positive value if there is inflow
!!
!!  USAGE:
!!       H = Equals H
!!       H Condition = Variable Normal Vector 1, Normal Vector 2, ssavelocity 1, ssavelocity 2
!!           REAL procedure "USFs" "HNormalCondition"
!!
!!   => Dirichlet condition (H = Equals H) is applied if there is inlow
! (ssavelocity.normal) < 0.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      FUNCTION HNormalCondition(Model,nodenumber,VarIn) RESULT(VarOut)
       USE Types
       implicit none
       !-----------------
       TYPE(Model_t) :: Model
       INTEGER :: nodenumber
       REAL(kind=dp) :: VarIn(4)
       REAL(kind=dp) :: VarOut
       !-----------------
       REAL(kind=dp) :: normal(2)
       REAL(kind=dp) :: velocity(2)

       normal(1:2)=VarIn(1:2)
       velocity(1:2)=VarIn(3:4)

       ! - velocity . normal; i.e. >0 for inflow
       VarOut=-SUM(velocity(:)*normal(:))

       END FUNCTION HNormalCondition

      FUNCTION HDirichlet(Model,nodenumber,VarIn) RESULT(VarOut)
       USE DefUtils
       implicit none
       !-----------------
       TYPE(Model_t) :: Model
       INTEGER :: nodenumber
       REAL(kind=dp) :: VarIn
       REAL(kind=dp) :: VarOut
       !-----------------
       TYPE(Variable_t),POINTER :: HVar
       INTEGER,POINTER :: HPerm(:)
       INTEGER :: k
       INTEGER :: ts

       ts = GetTimeStep()

       HVar =>  VariableGet( Model %Mesh % Variables,"H", UnFoundFatal=.TRUE.)
       HPerm => HVar % Perm(:)
       IF (.NOT.ASSOCIATED(HPerm)) &
          CALL FATAL('HDirichlet','HPerm not associated')

       k=HPerm(nodenumber)
       IF (k.LE.0) &
          CALL FATAL('HDirichlet','Wrong permutation?')
       IF ((ASSOCIATED(HVar%PrevValues)).AND.(ts>1)) THEN
          VarOut=HVar%PrevValues(k,1)
       ELSE
          VarOut=HVar%Values(k)
       ENDIF

       END FUNCTION HDirichlet
