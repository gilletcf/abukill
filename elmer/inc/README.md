# Parmeter files

## Content:
- [abuk.inc.sif](abuk.inc.sif): Main parameters for the simulation
	- number of time-steps
	- data files ...
- [mesh_adapt.inc.sif](mesh_adapt.inc.sif): mesh adaptation parameters
- [elmer.param](aelmer.param): the Phyiscal parameters (densities, etc..)
- [proj.inc.sif](proj.inc.sif): the current projection
