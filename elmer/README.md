# ABUKILL configuration

scripts and configurations files to run the ABUKILL experiment.

The surface mass balance is constant and we don't need basal mass balance under floating ice as
floating elements arte killed.

## Usage

1. Get required data-sets:   
	- bedrock elevation (usually form BedMachine Antarctica)   
	- smb forcing (usually from a regional climate model)   
	- elmer constant parameter fields: friction coefficient and viscosity (usually from an initialisation)   
	- elmer initial state (zs and ssavelocity) (usually after a relaxation)   
2. Update/Create a directory with templates for job submission for you scheduler
3. Check/Update the .sif files; e.g. 
	- *abuk_i_j_k.sif*: check the friction law and time-step
	- *init_i_j.sif*: update how the data are interpolated onto the mesh
4. Create and move to a working directory
5. Copy/Update a configuration file; see the template [Run.param.sh]()
6. Check/Update parameter files in the [inc](inc) directory; Eventually copy/update this directory under the working directory to make it specific for this experiment   
7. copy the scripts from [scripts](scripts)   
8. Run the initialisation by submitting a job that will run the script *MakeRun_init.sh*; see e.g. MSUB/Job_init.sh


## Content

- [Run.param.sh](Run.param.sh): Template parameter file

- [scripts](scripts): bash scripts to drive the simulations
- [sif](sif): elmer .sif files
- [inc](inc): generic parameter files
- [src](src): elmer source codes developped for this configuration
- [xios](xios): configuration files for the outputs using xios
- [MSUB](MSUB): job submissions files for TGCC
