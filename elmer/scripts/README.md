# Simulation scripts

## Content

- [MakeRun_i_j_k.sh](MakeRun_i_j_k.sh): main driver to run step <i> <j> <k>
- [MakeRun_init.sh](MakeRun_init.sh): initialisation script to compile and copy required files to run the current experiment
