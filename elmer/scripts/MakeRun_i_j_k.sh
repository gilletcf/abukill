#!/bin/bash
##########################################################################
# Run current simulation step <i> <j> <k>:
# - create step.inc.sif with the current values
# - if (k==1):
#	- run init.sif: interpolate results from the previous time interval
# - run abuk_i_j_k.sif: main physical simulation
# - if (k==kmax):
#	- run adapt_i_j.sif: mesh adapatation
#		- if (j==jmax) or (converged): adapt mesh using the last state metric
#		- else: adapt mesh using the transient metric
# - submit next job
###########################################################################
set -e

##################################
##################################
function usage {
   echo "USAGE: "
   echo "Run current simulation step <i> <j> <k>"
   echo  "> ./MakeRun_i_j_k.sh -i <i> -j <j> -k <k>"
}

function check_parameters {
  ## current step ii and jj
  if [[ -z "$ii" ]] || [[ -z "jj" ]] || [[ -z "kk" ]] ; then
	echo "MakeRun_i_j_k.sh: one of the parameter <i>, <j> ok <k> is empty"
	echo
	usage
	exit 1
  fi

  ## current exp.name (set in Run.param.sh)
  if [[ -z "$exp_name" ]]; then
	  echo "MakeRun_i_j_k.sh: parameter <exp_name> is empty"
	  exit 1
  fi

  ## max number of physical steps 
  # (set in Run.param.sh)
  if [[ -z "$imax" ]]; then
          echo "MakeRun_i_j_k.sh: parameter <imax> is empty"
          exit 1
  fi
  if [[ $ii -gt $imax ]]; then
	  echo "MakeRun_i_j_k.sh: error current step i $ii > $imax"
	  exit 1
  fi


  ## max number of adaptation steps
  # (set in Run.param.sh)
  if [[ -z "$jmax" ]]; then
          echo "MakeRun_i_j_k.sh: parameter <jmax> is empty"
          exit 1
  fi
  if [[ $jj -gt $jmax ]]; then
	  echo "MakeRun_i_j_k.sh: error current step j $jj > $jmax"
	  exit 1
  fi

  ## max number of steps k
  # (set in Run.param.sh)
  if [[ -z "$kmax" ]]; then
          echo "MakeRun_i_j_k.sh: parameter <kmax> is empty"
          exit 1
  fi
  if [[ $kk -gt $kmax ]]; then
	  echo "MakeRun_i_j_k.sh: error current step j $kk > $kmax"
	  exit 1
  fi

  ## top dir. with source files
  # (set in Run.param.sh)
  if [[ -z "$TOPDIR" ]]; then
	  echo "MakeRun_i_j_k.sh: parameter <TOPDIR> is empty"
          exit 1
  fi

  ## number of partitions to run abuk.sif
  # (set in Run.param.sh)
  if [[ ! "$np1" =~ ^[0-9]+$ ]]; then
          echo "MakeRun_i_j_k.sh: parameter np1: $np1 is not an integer"
          exit 1
  fi
  ## number of partitions to run init.sif
  # (set in Run.param.sh)
  if [[ ! "$np2" =~ ^[0-9]+$ ]]; then
          echo "MakeRun_i_j_k.sh: parameter np2: $np2 is not an integer"
          exit 1
  fi

}


##################################
##################################
## args processing
##################################
##################################
while [[ $# -gt 0 ]]; do
  case $1 in
    -i)
      ii="$2"
      shift # past argument
      shift # past value
      ;;
    -j)
      jj="$2"
      shift # past argument
      shift # past value
      ;;
    -k)
      kk="$2"
      shift # past argument
      shift # past value
      ;;
    -h|--help)
      usage
      shift
      ;;
    *|-*|--*)
      echo "MakeRun_i_j_k.sh: Unknown option $1"
      echo
      usage
      exit 1
      ;;
  esac
done

##################################
##################################
## Load generic run parameters
##################################
##################################
source Run.param.sh

##################################
##################################
## Check that required parameters
## have been set
##################################
##################################
check_parameters

##################################
## print some info
##################################
echo
echo "MakeRun_i_j_k.sh: ######################################"
echo "MakeRun_i_j_k.sh: current step i="$ii" j="$jj" k="$kk" "


#######################################
# current iteration steps
#######################################
echo "!# Current iteration:" >  step.inc.sif
echo "#II=\"$ii\"" >> step.inc.sif
echo "#JJ=\"$jj\"" >> step.inc.sif
echo "#KK=\"$kk\"" >> step.inc.sif


#######################################
# Check current mesh exists
#######################################
MESH_DIR=MSH_"$ii"_"$jj"
if [[ ! -d  "$MESH_DIR" ]]; then
	  echo "current mesh directory $MESH_DIR not existing;"
	  exit 1
fi


#########################################
## k=1 => need to produce result_i_j_0.nc
## from previous physical i-loop
#########################################
if [[ $kk -eq 1 ]]; then
   ##################################
   ## get the previous final state 
   ## to restart from
   ##################################
   i_prev=$((ii-1))
   j_prev=$(ls -1q result_"$exp_name"_"$i_prev"_*_"$kmax".nc | wc -l)
   if [[ $j_prev -lt 1 ]]; then
          echo "MakeRun_i_j_k.sh: there is no result file from previous physical step "$i_prev""
          exit 1
   fi
   result_file=result_"$exp_name"_"$i_prev"_"$j_prev"_"$kmax".nc
   if [[ ! -f "$result_file" ]]; then
       echo "MakeRun_i_j_k.sh: expected result file >$result_file< not found"
       exit 1
   fi

   echo "MakeRun_i_j_k.sh: Restart state from : "$result_file" "
   echo "MakeRun_i_j_k.sh: ######################################"
   echo

   echo "!# Previous completed step i-1 jmax kmax:" >  init.inc.sif
   echo "#IPREV=\"$i_prev\"" >> init.inc.sif
   echo "#JPREV=\"$j_prev\"" >> init.inc.sif
   echo "#KPREV=\"$kmax\""   >> init.inc.sif


   #######################################
   # mesh partitioning if required
   #######################################
   # run init in np2 partition2;
   # no halo required; 
   if [ ! -d $MESH_DIR/partitioning.$np2 ]; then
      ElmerGrid 2 2 $MESH_DIR -metis $np2
   fi


   #######################################
   ## init current .sif file
   #######################################
   cp  ${TOPDIR}/sif/init_i_j.sif init_"$ii"_"$jj".sif
   echo init_"$ii"_"$jj".sif > ELMERSOLVER_STARTINFO
   #######################################
   ## copy xios files
   #######################################
   cp ${TOPDIR}/xios/iodef.xml .
   cp ${TOPDIR}/xios/context_elmerice.init.xml context_elmerice.xml
   #######################################
   ## run Elmer
   #######################################
   run_elmer_xios $np2 $nxios

   echo "done" > init_"$ii"_"$jj".success

   ######################################
   ## get correct time from last time in previous result file
   ######################################
   ncks -A -F -d time,-1,-1 -v time $result_file result_"$exp_name"_"$ii"_"$jj"_0.nc
fi


#######################################
# RUN MAIN SIMULATION
#######################################

#######################################
# mesh partitioning if required
#######################################
# run abuk in np1 partitions
if [ ! -d $MESH_DIR/partitioning.$np1 ]; then
   ElmerGrid 2 2 $MESH_DIR -metis $np1 -halogreedy
fi

#######################################
## abuk current .sif file
#######################################
cp  ${TOPDIR}/sif/abuk_i_j_k.sif abuk_"$ii"_"$jj"_"$kk".sif
echo abuk_"$ii"_"$jj"_"$kk".sif > ELMERSOLVER_STARTINFO
#######################################
## copy xios files
#######################################
cp ${TOPDIR}/xios/iodef.xml .
cp ${TOPDIR}/xios/context_elmerice.abuk.xml context_elmerice.xml
#######################################
## run Elmer
#######################################
run_elmer_xios $np1 $nxios
echo "done" > abuk_"$ii"_"$jj"_"$kk".success


if [[ $kk -lt $kmax ]]; then
  i_next=$ii
  j_next=$jj
  k_next=$((kk+1))
   
else

   ##################################
   ## next step
   ##################################
   if ( [ $jj -eq $jmax ] || converged $ii $jj ) ; then
      i_next=$((ii+1))
      j_next=1
      k_next=1
   else
      i_next=$ii
      j_next=$((jj+1))
      k_next=1
    fi

   ##################################
   ## move to next step
   ##################################
   echo "!# Next step :" >>  step.inc.sif
   echo "#INEXT=\"$i_next\"" >> step.inc.sif
   echo "#JNEXT=\"$j_next\"" >> step.inc.sif

   if [[ $j_next -eq 1 ]]; then
	   echo "#MVarName=\"m1m2\"" >> step.inc.sif
   else
	   echo "#MVarName=\"mt\""   >> step.inc.sif
   fi

   #######################################
   ## Mesh adaptation
   ## (we could skip the last mesh adaptation
   ##  but if we want to continue it's better to have the mesh done)
   #######################################
   cp ${TOPDIR}/sif/adapt_i_j.sif adapt_"$ii"_"$jj".sif
   #######################################
   ## run Elmer
   #######################################
   rm -f *.xml
   ElmerSolver adapt_"$ii"_"$jj".sif

   ## check that the mesh dir. has been produced
   MESH_DIR=MSH_"$i_next"_"$j_next"
   if [[ ! -d  "$MESH_DIR" ]]; then
        echo "next mesh directory $MESH_DIR not existing;"
        exit 1
   fi
   ## ifort tends to add empty lines.... remove them
   sed -i '/^[[:space:]]*$/d' $MESH_DIR/mesh.nodes

   echo "done" > adapt_"$ii"_"$jj".success

   ##################################
   ## i_next > $imax; we are done
   ##################################
   if [[ $i_next -gt $imax ]]; then
	echo
        echo "MakeRun_i_j_k.sh: ######################################"
        echo "MakeRun_i_j_k.sh: THE END..."
        echo "MakeRun_i_j_k.sh: ######################################"
        echo
        exit 0
   fi
fi

submit_next_job $i_next $j_next $k_next
