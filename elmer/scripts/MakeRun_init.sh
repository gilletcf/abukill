#!/bin/bash
#######################################
# Initialisation script:
#  - compile required codes 
#  - copy generic inc files:
#	- from the main top dir inc folder
#	- or if existing from the current directory
#  - copy initial mesh and restart file
#  - submit first job
#######################################
set -e

#######################################
# experiments parameters
#######################################
source Run.param.sh

#######################################
## Generic initialisation
#######################################

#######################################
# Compile required codes
#######################################
export SRC_DIR=${TOPDIR}/src
make -f $SRC_DIR/Makefile

#######################################
# get a local copy of xios
#######################################
cp $XIOS_BIN/xios_server.exe .

#######################################
# copy required inc files
#######################################
# first copy inc files from main dir
cp ${TOPDIR}/inc/* .
# copy inc file from current dir;
# i.e. allow to overwrite default
if [ -d inc ]; then
   cp inc/* .
fi

## update abuk.inc with expname provided from Run.param
sed -i "s/<exp_name>/$exp_name/g" abuk.inc.sif

#######################################
## INITIAL STATE AND MSH
#######################################

#######################################
# mimic step 0 1 kmax
####################################### 
ii=0
jj=1
kk=$kmax

i_next=1
j_next=1

#######################################
# copy initial state file and mesh
# with required naming
#######################################
if [[ -z "$INIT_MESH" ]]; then
          echo "MakeRun_init.sh: parameter <INIT_MESH> is empty"
          exit 1
fi
if [[ -z "$INIT_FILE" ]]; then
          echo "MakeRun_init.sh: parameter <INIT_FILE> is empty"
          exit 1
fi
cp -r $INIT_MESH MSH_"$ii"_"$jj"
cp $INIT_FILE result_"$exp_name"_"$ii"_"$jj"_"$kk".nc

##################################
# Create starting mesh MSH_1_1
# adpat solution to initial state
## or we could copy MSH_0_1...
##################################
if $first_adapt ; then
   echo "!# Current iteration:" >  step.inc.sif
   echo "#II=\"$ii\"" >> step.inc.sif
   echo "#JJ=\"$jj\"" >> step.inc.sif
   echo "#KK=\"$kk\"" >> step.inc.sif
   echo "!# Next step :" >>  step.inc.sif
   echo "#INEXT=\"$i_next\"" >> step.inc.sif
   echo "#JNEXT=\"$j_next\"" >> step.inc.sif

   ElmerGrid 2 2 MSH_"$ii"_"$jj" -metis $np2
   rm -f iodef.xml
   cp  ${TOPDIR}/sif/adapt_0_1.sif .

   echo adapt_0_1.sif > ELMERSOLVER_STARTINFO
   run_elmer $np2

   ## ifort tends to add empty lines.... remove them
   sed -i '/^[[:space:]]*$/d' MSH_1_1/mesh.nodes

else
   cp -r MSH_"$ii"_"$jj" MSH_1_1
fi


#################################
# and run step 1 1 1
#################################
submit_next_job 1 1 1
