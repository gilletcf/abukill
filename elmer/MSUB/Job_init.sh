#!/bin/bash
###########################
### Job dependency########
###########################
#MSUB -o run_0_1.%j.o
#MSUB -e run_0_1.%j.e
#MSUB -n 128
#MSUB -T 600 (walltime in second)
#MSUB -A gen6066 
#MSUB -q rome (partition name, rome for us)
#MSUB -m work,scratch (spaces accessible from the compute node)
#MSUB -x

set -x

ulimit -s unlimited

cd ${BRIDGE_MSUB_PWD}

JOB_NAME=RUN_1_1

rm -f  ${JOB_NAME}_SUCCESS.txt ${JOB_NAME}_FAILURE.txt
echo $BRIDGE_MSUB_JOBID > ${JOB_NAME}_RUN.txt

###
./MakeRun_init.sh
##
## exit status
if [ $? -eq 0 ]
then
  echo $BRIDGE_MSUB_JOBID > ${JOB_NAME}_SUCCESS.txt
else
  echo $BRIDGE_MSUB_JOBID > ${JOB_NAME}_FAILURE.txt
fi

rm -f ${JOB_NAME}_RUN.txt

