#!/bin/bash
###########################
#MSUB -o run_<II>_<JJ>_<KK>.%j.o
#MSUB -e run_<II>_<JJ>_<KK>.%j.e
#MSUB -n 128
#MSUB -A gen6066 
#MSUB -q rome (partition name, rome for us)
#MSUB -m work,scratch (spaces accessible from the compute node)
#MSUB -x

set -x

ulimit -s unlimited

cd ${BRIDGE_MSUB_PWD}

JOB_NAME=RUN_<II>_<JJ>_<KK>

rm -f  ${JOB_NAME}_SUCCESS.txt ${JOB_NAME}_FAILURE.txt
echo $BRIDGE_MSUB_JOBID > ${JOB_NAME}_RUN.txt

#### run job
./MakeRun_i_j_k.sh -i <II> -j <JJ> -k <KK>


## exit status
if [ $? -eq 0 ]
then
  echo $BRIDGE_MSUB_JOBID > ${JOB_NAME}_SUCCESS.txt
else
  echo $BRIDGE_MSUB_JOBID > ${JOB_NAME}_FAILURE.txt
fi

rm -f ${JOB_NAME}_RUN.txt

