# Job submission scripts for TGCC

## Content
- [Job_init.sh](Job_init.sh): run initialisation script *MakeRun\_init.sh*
- [Job.tp.sh](Job.tp.sh): 
	- template script to run step <i> <j> <k>
	- called by the function *submit_next_job()* in *Run.param.sh*
