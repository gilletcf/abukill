# xios configuration files

## Content
- [iodef.xml](iodef.xml): main xios config. file
- [context_elmerice.abuk.xml](context_elmerice.abuk.xml): context file for abuk\_i\_j\_k.sif 
- [context_elmerice.init.sif](context_elmerice.init.sif): context file for init\_i\_j.sif
