!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
include elmer.param
include abuk.inc.sif
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
include step.inc.sif
include init.inc.sif
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Header
  Mesh DB "." "MSH_#II#_#JJ#"
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Constants
  !Physical
  Sea Level = Real $zsl
  Water Density = Real $rhow
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Simulation

  include proj.inc.sif 

  Coordinate System  = Cartesian 2D

  Simulation Type = Steady State

  Steady State Min Iterations = 1
  Steady State Max Iterations = 1

  ! Variable for interpolation
  Interpolation Global Epsilon = Real 2.0d-10
  Interpolation Local Epsilon = Real 1.0d-10
  Interpolation Max Iterations = Integer 1

  max output level = 3
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Body 1
  Equation = 1
  Material = 1
  Body Force = 1
  Initial Condition = 1
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Initial Condition 1
  !---------------------------------------------
  ! MESH ADAPTATION
  !---------------------------------------------
  Mt 1 = Real $1.0/1.0e10
  Mt 2 = Real $1.0/1.0e10
  Mt 3 = Real 0.0
End

Body Force 1
  
End

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Material 1
  SSA Mean Density = Real $rhoi
End

!#-----------------------------------------------------------------------
!#                          READ SMB
!# - use grided data for now; as it will require conservative interpolation if remeshing...
!#-----------------------------------------------------------------------
Solver 1
   Exec Solver = Before simulation

   Equation = "SMBref_DataReader"
   Procedure = "GridDataReader" "GridDataReader"
   Variable = -nooutput dumy

  !---- NOTE: File is case sensitive, String is not!
   FileName = File "#file_smb#"

  !Time Dim Name = String "time"
  X Dim Name = File "x"
  Y Dim Name = File "y"

  !Time Var Name = String "time"
  X Var Name = File "x"
  Y Var Name = File "y"

  !
  Read full array = Logical True

  !- Variables to read
   Variable 1 = File "SMB"
   Target Variable 1 = String "smb"

   Interpolation Multiplier = Real # 1.0/365.0
   Interpolation Offset = Real 0.0

End   

!#-----------------------------------------------------------------------
!#                          READ Bedrock
!#-----------------------------------------------------------------------
Solver 2
   Exec Solver = Before simulation

   Equation = "Bedrock_DataReader"
   Procedure = "GridDataReader" "GridDataReader"
   Variable = -nooutput dumy

  !---- NOTE: File is case sensitive, String is not!
   FileName = File "#file_bed#"

  !Time Dim Name = String "time"
  X Dim Name = File "x"
  Y Dim Name = File "y"

  !Time Var Name = String "time"
  X Var Name = File "x"
  Y Var Name = File "y"

  !
  Read full array = Logical True

  !- Variables to read
   Variable 1 = File "bed"
   Target Variable 1 = String "bedrock"

End   

!#-----------------------------------------------------------------------
!#                          READ PARAMETERS
!#-----------------------------------------------------------------------
Solver 3
   Exec Solver = Before Simulation
   Equation = "SSAParameters_DataReader"
   Procedure = "ElmerIceSolvers" "UGridDataReader"

   mesh = -single  "#MSH_INIT#"
   UnFoundNodes Fatal = Logical FALSE

   File Name = File "#file_init#"

!# Read last time frame of the restart
   Time Index = Integer -1

   Variable Name 1  = File "mueta2"
   Variable Name 2 = File "beta_1"

End

!#-----------------------------------------------------------------------
!#                          READ PARAMETERS
!#-----------------------------------------------------------------------
Solver 4
   Exec Solver = Before Simulation
   Equation = "UGridDataReader"
   Procedure = "ElmerIceSolvers2" "UGridDataReader"

   mesh = -single  "MSH_#IPREV#_#JPREV#"
   UnFoundNodes Fatal = Logical FALSE

   File Name = File "result_#exp_name#_#IPREV#_#JPREV#_#KPREV#.nc"

!# Read last time frame of the restart
   Time Index = Integer -1

   Variable Name 1  = File "zs"
   Variable Name 2 = File "ssavelocity 1"
   Variable Name 3 = File "ssavelocity 2"

End

!#-----------------------------------------------------------------------
!#                          FLOTATION SOLVER
!#-----------------------------------------------------------------------
Solver 5
   Exec Solver = Before Simulation
   Equation = "Flotation"
   Procedure = "MyFlotation" "Flotation"
   ! primary variable
   Variable = GroundedMask

  ! compute zb and H from bedrock and zs
   Use zs as input = Logical True

   ! Create required variables
   Exported Variable 1 = smb
   Exported Variable 2 = bedrock
   Exported Variable 3 = mueta2
   Exported Variable 4 = beta_1
   Exported Variable 5 = zs
   Exported Variable 6 = -dofs 2 "ssavelocity"
   Exported Variable 7 = H
   Exported Variable 8 = zb
   Exported Variable 9 = -dofs 3 "Mt"
End

!------------------------------
!- Save results wit XIOS
!------------------------------
Solver 6
   Exec Solver = After timestep

   Equation = "XIOSOutPutSolve"
   Procedure = "ElmerIceSolvers" "XIOSOutputSolver"

   time_units=String "1d"

  ! to set the start date from elmer; start date will be reference date + (Gettime()-dt)*time_units
   reference date=String "#reference_date#"

  ! output frequency
   output frequency = String "1ts"
   sync frequency = String "1ts"

! name format
   file names suffix = File "_#exp_name#_#II#_#JJ#_0"

! node and elem vars
   Scalar Field 1 = String "bedrock"
   Scalar Field 2 = String "zb"
   Scalar Field 3 = String "zs"
   Scalar Field 4 = String "h"
   Scalar Field 5 = String "groundedmask"

   Scalar Field 6 = String "ssavelocity 1"
   Scalar Field 7 = String "ssavelocity 2"
   Scalar Field 8 = String "mueta2"
   Scalar Field 9 = String "beta_1"

 
   Scalar Field 10 = String "smb"  

 ! mesh adaptation stuff
   Scalar Field 11 = String "Mt 1"
   Scalar Field 12 = String "Mt 2"
   Scalar Field 13 = String "Mt 3"


   Solver info level = integer 3
End

!#####
Equation 1
  Active Solvers (6)= 1 2 3 4 5 6
End

!#####
Boundary Condition 1
  Name = "Ice Front"
  Target Boundaries = 1

End


