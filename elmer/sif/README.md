# elmer configuration files

## Content

- [init_i_j.sif](init_i_j.sif): initialisation for step <i> <j> <k=1>
	- import data onto the new mesh
	- import final state from the last time interval
- [abuk_i_j_k.sif](abuk_i_j_k.sif): run the abukill experiment
- [adapt_i_j.sif](adapt_i_j.sif): run the mesh adaptation
- [adapat_0_1.sif](adapt_0_1.sif): initial mesh adaptation
