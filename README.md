# ABUKILL

## Description:

[Elmer/Ice](http://elmerice.elmerfem.org/) configuration for the **ABUK** experiments from the ABUMIP intercomparison project ABUMIP (Sun *et al.*, 2020).  

The configuration derives from the [ISMIP6 Antarctica configuration](https://github.com/pmathiot/ELMER_ISMIP6_Antarctica)

We implement a transient fixed point mesh adaptation algorithm (Alauzet *et al.*, 2007) to insure sufficient mesh resolution as the simulation (and the grounding line) evolves/

### References:  

- Sun S, Pattyn F, Simon EG, et al. Antarctic ice sheet response to sudden and sustained ice-shelf collapse (ABUMIP). Journal of Glaciology. 2020;66(260):891-904. <doi:10.1017/jog.2020.67>
- Alauzet, F., Frey, P.J., George, P.L., Mohammadi, B., 2007. 3D transient fixed point mesh adaptation for time-dependent problems: Application to CFD simulations. Journal of Computational Physics 222, 592-623. <https://doi.org/10.1016/j.jcp.2006.08.012>


## Details on the implementation of the **ABUK** experiments

From Sun et al. (2020):   
- *Ice-shelf removal or 'float-kill' (ABUK)*
- *For the first forcing experiment, all floating ice (ice shelves) surrounding the ice sheet was removed at the start of the run and thereafter any newly-formed floating ice was instantaneously removed (so-called 'float-kill'). In other words, at all times, calving flux was assumed to be larger than the flux across the grounding line to prohibit regrowth of the shelves.*


We use the active/passive mechanism implemented in Elmer. Because killing floating elements leaves active small pinning points and islands that are grounded and that are likely to become instable, we first detect thoses areas using the solver **GetPassive**. The solver detects elements of the mesh that are active and connected at least by one edge. The area and number of elments in each connected group are computed and groups where the values are below the respective given thresholds are also made passive. The initial condition for an element to become passive is that all groundedmask values must be equal to -1, i.e. the element is entirely floating.


```
Body Force 1
  gp Passive =  Variable groundedmask
    Real Procedure "USFs" "abuk"
    
  Passive Element Min Nodes = Integer 3
End
    
 Solver 2
  Exec Solver = Before timestep
  Equation = "GetPassive"
  Variable = -dofs 1 "gp"

   procedure = "GetPassive" "GetPassive"

  !solver kws; remove regions that are less than 30km*30km or 10 elements
  Area Lower Limit = Real 9.0e8
  Element Lower Limit = Integer 10
End
```

The solver returns a variable defined on the element called **passive**. The variable is equal to **+1** if the element has to be passive. This variable can thus be used for the other solvers; e.g. *SSASolver* and *ThicknessSolver*.

```
Body Force 1
  SAVelocity Passive = Equals Passive
  SSAVelocity 1 = Real 0.0
  SSAVelocity 1 Condition Passive = Logical True
  SSAVelocity 2 = Real 0.0
  SSAVelocity 2 Condition Passive = Logical True

  H Passive = Equals Passive
  H = Real $Hmin
  H Condition Passive = Logical True
End
```

The new boundary between the active and passive elements is automatically computed within Elmer and the correspondig boundary conditions can be prescribe on this boundary; here the *calving front* boundary condition for the SSASolver:
```
Boundary Condition 2
  Passive Target = Logical True

  Calving Front = Logical True
End
```

Remarks:
- In parallel, for the active/passive mechanism to work, in case a passive/active boundary correspond to the boundary between partitions, the mesh must contain **halo** elements. Experiences have shown that greedy halo elements are required; this can be done when partitionning the mesh with Elmer as, e.g.
```
ElmerGrid 2 2 *MESH_DIR* -metis *np* -halogreedy
```
- By default in Elmer, variables defined as element variable in the .sif file using
```
Exported Variable 1 = -elem MyVar
```
receive a permutation table based the the *active* elements; here active means elements that belong to the current partition. The variable **passive** is created internally and is thus defined on all the bulkelements, including the halos and thus can be subsequently used as a condition for the passive/active mechanism.


## Details on the implementation of the mesh adaptation algorithm

Backround material in elmerice on the transient mesh adaptation:

-   the *Anisotropic Mesh Adaptation* material in the 2017 **Advanced Elmer/Ice Workshop** which was held at IGE in Grenoble:\
    <http://elmerice.elmerfem.org/wiki/lib/exe/fetch.php?media=courses:2017:mesh_adapt_2017.pdf>
-   Implementation adapted from the elmerice sources:   
    -   <https://github.com/ElmerCSC/elmerfem/blob/devel/elmerice/Solvers/MeshAdaptation_2D/Script_Transient.sh>\
    -   <https://github.com/ElmerCSC/elmerfem/tree/devel/elmerice/Tests/MMG2D_Transient>

The basic idea is to iterate over the transient simulation and to adapt the mesh using information from the transient simulation to insure that phenomena of interest are always captured with a sufficient resolution. The "error estimate" to compute the metric field is based on an upper bound for the interpolation error and thus depends on the hessian matrix of the variable of interest. Here we combine information from the velocity norm and ice thickness. The metrics are computed at each time step (eventually this could be done less frequently to save computing ressources) and combined together and in time, using a metric intersection scheme.

Here, the total simulation duration from *t_0* to *t_f* is splitted in *N_i* intervals (**i-loop**).
For each interval,  run the transient mesh adaptation (**j-loop**). The interval can be splitted in several sub-intervals for check-pointing (**k-loop**). 

The implement use 3 elmer configuration .sif files: [**init_i_j.sif**](elmer/sif/init_i_j.sif),  [**adapt_i_j.sif**](elmer/sif/adapt_i_j.sif) and [**abuk_i_j_k.sif**](elmer/sif/abuk_i_j_k.sif).

The workflow is driven by one bash script file: [**MakeRun_i_j_k.sh**](elmer/scripts/MakeRun_i_j_k.sh)

Algorithm:
```
for i=1,imax :  Physical simulation over the entire duration from t_0 to t_f
	- **init_i_j.sif** - variables initialisation for the start of the current interval
		- interpolate initial state to current **i** **j** mesh:
			1. interpolate forcings and bedrock from input data-sets
			2. interpolate friction and viscosity from reference inversion grid
			3. interpolate "state" variables *ssavelocity* and *zs* from the final result of the previous interval 

	- for j=1,jmax : Mesh adaptation loop;
	   - **adapt_i_j.sif** : mesh adaptation 
	   - j=1:    
	      - adapt mesh using last "state" metric
	   - j> 1:  
	      - adapt mesh from the previous "transient" metric computed during j-1

	   - for k=1,kmax: Physical simulation for current interval **i** doing check-point restart every *ndt*:
	   	**abuk_i_j_k.sif**  - the physical ice-sheet simulation:
			- restart previous result file **k-1**
			- for each *ts* compute:
				1. Get passive/active mask
				2. Main ts coupling
					- SSA Solver
					- Thickness Solver
					- Flotation
				3. Compute Mesh adaptation metric from current solution
				4. Metric Intersection in time
	 - If converged move to next interval **i+1**	
	 
```

Remarks:   
- Using only one interval (**imax=1**) might result in a very large mesh if there is large dispalcement of the grounding line, requiring high resolution in large areas.
- Increasing the number of interval requires to interpolate the solutions; and thus introduce some smoothing and discontinuity in the simulation.
- For the moment we use bi-linear interpolation to import data to the current mesh and between elmer meshes.
- see [ElmerUgrid](https://gricad-gitlab.univ-grenoble-alpes.fr/gilletcf/elmerugrid) for python codes and conservative interpolation.
- For a slightly different mesh adaptation scheme; might be interesting to have a look to Alauzet and Olivier, [An L∞-Lp space–time anisotropic mesh adaptation strategy for time-dependent problems](https://www.researchgate.net/publication/267683112_An_L-Lp_space-time_anisotropic_mesh_adaptation_strategy_for_time-dependent_problems). Especially as the continuous framework introduce a normalisation to control the mesh size; while here it's controlled by the *a-priori* prescribed errors. So might result in large meshes.

## Content

- [elmer](elmer): elmer configurations files and codes
- [results](results): some results of the performed experiments
